Créer un repository dans le groupe qui vous a été alloué.

Structure du repo attendue :

 - part1 
 - part2

Heure limite du dernier commit : **11h45** 
Les évaluations rendues au-delà ne sont pas acceptées

![Design à implémenter](./specifications/1_Design.png)

***
Partie 1
========

Pour le squelette de base de l'appli voir le répertoire part1. Le package.json est déjà rempli avec les dépendances nécessaires (jquery, font-awesome, bootstrap). Il suffit juste de les installer.

### HTML5 
_2 points_
***
* Déclarer le DOCTYPE, le charset UTF-8, le titre, 1 feuilles de style (`custom.css` par exemple), 1 fichier javascript (`app.js` par exemple). Pas de fichier fourre-tout avec le css, le js et le html !!
* Mettre en place la structure HTML correspondante aux maquettes **en utilisant les balises HTML5** (header, main, figure, article, section)
* Choisir la hiérarchie de titre adéquat selon la spécification HTML5 (h1, h2, etc.)
* Utiliser les nouveaux attributs de form HTML5 (required, placeholder, autofocus) et les nouveaux types de champs pour la recherche et le slider de prix

### CSS3 & Framework CSS (Bootstrap)
_3 points_
***
* Mettre en place le design correspondant aux maquettes **en utilisant autant que possible le framework Bootstrap 4**
* Les largeurs doivent être définies avec les classes `container-fluid`, `row`, `col-md-X`, `col-xs-X`, etc.
* Faire un dégradé du blanc au gris `#EFEFEF` sur l'en-tête, avec une ombre externe
* Faire un arrondi sur le champ de recherche, avec une ombre interne
* Faire une bordure arrondie autour de chaque produit
* Mettre en place les boutons `Description` et `Ajouter au panier` en utilisant les styles pré-définis de `Bootstrap 4`. Les icone sont mises en place en utilisant `Font-awesome`
* Utiliser la police `Roboto` pour tous les textes ( cf. répertoire font)

### Responsive web design
_5 points_
***
> Utiliser autant que possible le framework **Bootstrap 4** pour gérer les aspects responsive

![maquette responsive](./specifications/2_Responsive/responsive.gif)

* Rendre les images des produits proportionnelles à l'espace disponible : la taille doit varier en fonction de la taille de la fenêtre (images flexibles)
* Exprimer toutes les tailles de police dans une unitée relative plutôt qu'absolue
* Réduire automatiquement tous les textes sur tablette
* Réduire davantage automatiquement tous les textes sur mobile
* Faire une mise en page pour les tablettes / mobiles correspondant aux maquettes :
 * Mettre en place une mise en page fluide, sur toute la largeur de l'écran disponible
 * **La présentation des produits passe de 3 colonnes à 2, puis à 1 colonne en fonction de la largeur de l'écran**
 * Centrer le logo et la barre de recherche sur tablettes et mobiles
 * Masquer le bouton `Description` sur tablette / mobile
* S'assurer que la présentation est propre à n'importe quelle résolution

### jQuery
_4 points_
***
* Afficher le détail du produit uniquement au clic sur le bouton `Description`
 *  Animation déplié / replié
* Afficher la valeur du slider de prix à côté du slider
* Afficher la gamme de prix et la recherche au dessus du listing des produits
* Rendre les champs de recherche opérationnels : 
 * Recherche par nom de produit (titre)
 * Recherche par prix maximum
* Afficher le nombre de produits correspondants à la recherche au dessus de la liste des produits
* Afficher "Aucun vin trouvé" si la recherche ne trouve aucune correspondance

> Conseils

* Eviter la duplication de code en jQuery (DRY: Don't Repeat Yourself)

***
Partie 2
========

**Angular ou React au choix**
_6 points_
***
A partir du code HTML crée dans la partie 1 créer deux composant. Un composant liste de produits et un composant produit.
La liste de produits affichera des composants produits issus d'un tableau json fourni (products.js)

Bonus
-----

 - Implémenter le tri de la partie 1 dans la partie 2 avec la technologie choisie 
 - Ajouter un tri par type de vin (rouge ou blanc) dans la partie 2 au travers d'un autre champs de recherche (réfléchir à l’expérience utilisateur)

### Code snippets
***
* Liste des vins : 
 * JQuery : voir fichier `products.txt` 
 * Angular ou React : voir fichier `products.js`
* jQuery : utiliser `each` pour parcourir chaque élément du DOM

```javascript
    // Example
    $('.items h3').each(function(){
        console.log($(this).text());
    });
```
* Slider de la barre de recherche  

```html
    <input type="range" min="0" max="50" value="25" step="5" />
```
* Filtrer un tableau d'objets json : 

```javascript
	const tableauAfiltrer;
	//filter renvoie un nouveau tableau avec les elements qui remplissent la condition du filtre. Ici le prix > 20€.
    const filtre = tableauAfiltrer.filter((unElementDuTableau)=>
	return unElementDuTableau.price > 20;
);
```